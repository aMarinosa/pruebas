package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class FacturaTest {
	static GestorContabilidad gestorPruebas;
	static Factura facturaPrueba;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorPruebas.getListaClientes().clear();
		gestorPruebas = new GestorContabilidad();
		facturaPrueba = null;
	}
	
	@Test
	public void testCalcularPrecioProducto1(){ // Probar que la prueba devuelve el valor esperado
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		float esperado = 7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testCalcularPrecioCantidadCero(){ // Probar qu� devuelve si multiplicas por 0
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		float esperado = 0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		
		assertEquals(esperado, actual, 0.0F);
	}
	
	@Test
	public void testCalcularPrecioCantidadPrecioNegativo(){ // Probar qu� devuelve si se calcula con un precio negativo
		facturaPrueba.setCantidad(2);
		facturaPrueba.setPrecioUnidad(-1.5F);
		float esperado = -3.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		
		assertEquals(esperado, actual, 0.0);
	}
	
	// 3 METODOS CALCULAR PRECIO TOTAL
	
	@Test
	public void testCrearPrimeraFactura() { // Crear que se crea la primera factura de una lista vac�a
		Factura primeraFactura = new Factura("1034", LocalDate.now(), "prod 1", 20.00F, 2, null);
		gestorPruebas.crearFactura(primeraFactura);
		boolean esperado = gestorPruebas.getListaFacturas().contains(primeraFactura);
		
		assertTrue(esperado);
	}
	
	@Test
	public void testCrearVariasFacturas() { // Probar a meter m�s de una factura a la vez
		Factura factura1 = new Factura("1034", LocalDate.now(), "prod 1", 20.00F, 2, null);
		gestorPruebas.crearFactura(factura1);
		Factura factura2 = new Factura("10784", LocalDate.now(), "prod 3", 20.00F, 2, null);
		gestorPruebas.crearFactura(factura2);
		int esperado = gestorPruebas.getListaFacturas().size();
		
		assertEquals(esperado, 2);
	}
	
	@Test
	public void testCrearFacturaCodigoRepetido(){ // Crear 2 facturas con el mismo c�digo
		Factura prueba = new Factura("1034", LocalDate.now(), "prod 1", 20.00F, 2, null);
		gestorPruebas.crearFactura(prueba);
		Factura prueba2 = new Factura("1034", LocalDate.now(), "prod 1", 20.00F, 2, null);
		gestorPruebas.crearFactura(prueba2);
		boolean actual = gestorPruebas.getListaFacturas().contains(prueba2.getCodigoFactura());
		
		assertFalse(actual);
	}
	
	// 3 METODOS CREAR FACTURA
	
	@Test
	public void testEliminarFacturaInexistente(){ // Que devuelva false si no existe la factura
		gestorPruebas.eliminarFactura("456798-A");
		
		boolean actual = gestorPruebas.getListaFacturas().contains("456798-A");
		
		assertFalse(actual);
	}
	
	@Test
	public void eliminarFacturaExistente(){ // Que devuelva true si existe la factura y es eliminada.
		Factura nuevoFactura = new Factura("456798-A", LocalDate.now(),"producto 1", 10.20F, 6, null);
		gestorPruebas.eliminarFactura("456798-A");
		boolean actual = gestorPruebas.getListaFacturas().contains(nuevoFactura);
		
		assertTrue(actual);
	}
	
	// 2 METODOS ELIMINAR FACTURA
	
	@Test
	public void testBuscarFacturaCodigoInexistente(){ // Buscar una factura con un c�digo de esta que no existe.
		String codigo = "3413h";		
		Factura actual = gestorPruebas.buscarFactura(codigo);
		
		assertNull(actual);
	}
	
	@Test
	public void testBuscarFacturaCodigoExistente(){ // Comprobar que no devuelve null al devolver una factura existente.
		gestorPruebas.getListaClientes().clear();
		Factura esperada = new Factura("123456A",LocalDate.now(),"producto 1", (float)20.20, 8, null);
		gestorPruebas.getListaFacturas().add(esperada);
		Factura actual = gestorPruebas.buscarFactura("123456A");
		assertNotNull(actual);
	}
	
	// 2 METODOS BUSCAR FACTURAS
	
	@Test
	public void testCalcularPrecioAnual() { //Calcular el precio anual
		gestorPruebas.getListaFacturas().clear();
		Factura factura1 = new Factura("456798-A", LocalDate.now(),"producto 1", 10.00F, 1, null);
		gestorPruebas.getListaFacturas().add(factura1);
		Factura factura2 = new Factura("879-A", LocalDate.now(),"producto 2", 10.00F, 1, null);
		gestorPruebas.getListaFacturas().add(factura2);
		
		
		float esperado = gestorPruebas.calcularPrecioAnual(LocalDate.now().getYear());
		
		assertEquals(esperado, 20.00F, 0.0);
	}
	@Test
	public void testCalcularPrecioAnualSinFacturas(){ // Que me devuelva 0 en el caso de no tener facturas.
		gestorPruebas.getListaClientes().clear();
		float precio = gestorPruebas.calcularPrecioAnual(LocalDate.now().getYear());
		float precioEsperado = 0.0F;
		
		assertEquals(precio, precioEsperado, 0.0F);
	}
	
	@Test 
	public void testCalcularPrecioAnualConAnyoPosterior(){ // Que me devuelva 0 en el caso de que el a�o pedido sea mayor al actual.
		  float actual = gestorPruebas.calcularPrecioAnual(LocalDate.now().getYear()+1);
		  
		  assertEquals(actual, 0.0F, 0.0);
		
	}
	
	// 3 METODOS CALCULAR PRECIO ANUAL
	
	@Test
	public void testBuscarfacturaMasCaraConFacturas(){ // De dos facturas, que me devuelva la que mayor precio tenga.
		float esperado = 2000.4F;
		Factura esperada = new Factura("123456A",LocalDate.now(),"producto 1", esperado, 8, null);
		gestorPruebas.getListaFacturas().add(esperada);
		Factura prueba1 = new Factura("1adf23456A",LocalDate.now(),"producto 3", 200.8F, 7, null);
		gestorPruebas.getListaFacturas().add(prueba1);
		float precioEsperado = gestorPruebas.facturaMasCara().getPrecioUnidad();
		
		assertEquals(precioEsperado, esperado, 0.0);
	}
	
	@Test
	public void testBuscarFacturaMasCaraConPreciosIguales() { // Busca la factura m�s cara habiendo dos iguales
		Factura factura1 = new Factura("123456A", LocalDate.parse("2015-10-11"), "p 1", 1.60F, 2, null);
		Factura factura2 = new Factura("123456A87a", LocalDate.parse("2005-05-21"), "p 2", 2.60F, 5, null);
		gestorPruebas.getListaFacturas().add(factura1);
		gestorPruebas.getListaFacturas().add(factura2);
		Factura actual = gestorPruebas.facturaMasCara();

		assertSame(factura1, actual);
	}
	
	@Test
	public void testBuscarfacturaMasCaraSinFacturas(){ //Buscar factura m�s cara sin facturas, debe dar null.
		Factura prueba = gestorPruebas.facturaMasCara();
		
		assertNull(prueba);
	}
	
	@Test
	public void testBuscarFacturaMasCaraConUnaFactura() { //Busca la factura m�s cara con s�lo 1 elemento
		Factura actual = new Factura("123456A", LocalDate.parse("2015-10-11"), "p 1", 1.60F, 2, null);
		Factura factura = gestorPruebas.facturaMasCara();

		assertSame(actual, factura);
	}
	
	// 4 METODOS BUSCAR FACTURA MAS CARA
	
	@Test
	public void testCalculoAnualSinRegistros() {
		float esperado = 0.0F;
		float actual = gestorPruebas.calcularPrecioAnual(2000);

		assertEquals(esperado, actual, 0.0);
	}

	@Test
	public void testFacturaAnualUnRegistro() { // Factura anual con 1 solo registro
		Factura factura = new Factura("123456A", LocalDate.parse("2015-10-11"), "p 1", 2.6F, 1, null);
		gestorPruebas.getListaFacturas().add(factura);
		float esperado = 2.6F;

		float actual = gestorPruebas.calcularPrecioAnual(2005);

		assertEquals(esperado, actual, 0.0F);
	}
	
	@Test
	public void testAnualVariosRegistros() { // Calculo anual de varios registros
		Factura factura = new Factura("123456A", LocalDate.parse("2015-10-11"), "p 1", 1.60F, 2, null);
		gestorPruebas.getListaFacturas().add(factura);
		Factura factura2 = new Factura("123456A", LocalDate.parse("2015-10-11"), "p 1", 1.60F, 2, null);
		gestorPruebas.getListaFacturas().add(factura2);
		float expected = 50;
		float actual = gestorPruebas.calcularPrecioAnual(2015);
		
		assertEquals(expected, actual, 0.0);
	}
	
	// 3 METODOS FACTURACION ANUAL
	
}
