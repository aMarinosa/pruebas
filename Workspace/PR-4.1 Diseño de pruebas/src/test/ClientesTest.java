package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class ClientesTest {

	Cliente esperado;
	Cliente actual;
	static GestorContabilidad gestorPruebas;

	@BeforeClass
	static public void cons(){
		gestorPruebas = new GestorContabilidad();
	}
	
	
	
	@Test
	public void testBuscarclienteInexistenteSinClientes(){ // Buscar un cliente que no existe sin tener clientes previamente, debe dar null
		
		gestorPruebas.getListaClientes().clear();
		String dni = "2345234";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		//
		assertNull(actual);
	}
	
	@Test
	public void testBuscarclienteInexistenteConClientes(){ // Buscar cliente inexistente despu�s de haber metido clientes, debe dar null
		
		String dni = "6884F";
		//buscar cliente inexsistente, debe dar null
		
		actual = gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	
	@Test
	public void testBuscarClienteExistente(){ // Buscar un cliente que existe, debe dar not null
		Cliente esperado = new Cliente("Fernando","1234F",LocalDate.now());
		gestorPruebas.getListaClientes().add(esperado);
		actual = gestorPruebas.buscarCliente("1234F");
		assertNotNull(actual);
	}
	
	// 3 METODOS BUSCAR CLIENTE
	
	@Test
	public void testAltaPrimerCliente(){ // Prueba para dar de alta el primer cliente, debe dar true
		gestorPruebas.getListaClientes().clear();
		Cliente nuevoCliente = new Cliente("Juana", "12456789a", LocalDate.now());
		gestorPruebas.altaCliente(nuevoCliente);
		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	
	@Test
	public void testAltaVariosClientes(){ // Dar varios clientes de alta, debe permitirlos todos
		Cliente nuevoCliente = new Cliente("Juana", "20205u", LocalDate.now());
		gestorPruebas.altaCliente(nuevoCliente);
		nuevoCliente = new Cliente("Pedro", "88867a", LocalDate.now());
		gestorPruebas.altaCliente(nuevoCliente);
		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
		
	}
	
	@Test
	public void testAltaClienteDniRepetido(){ // Probar a dar de alta un cliente con el mismo dni que otro, debe dar null
		Cliente clienteDNI = new Cliente("Mar", "88896", LocalDate.now());
		gestorPruebas.altaCliente(clienteDNI);
		Cliente clienteDNIPrueba = new Cliente("Jose", "88896", LocalDate.now());
		gestorPruebas.altaCliente(clienteDNIPrueba);
		boolean actual = gestorPruebas.getListaClientes().contains(clienteDNIPrueba);
		
		assertFalse(actual);
	}
	
	// 3 METODOS DAR ALTA CLIENTE
	
	@Test
	public void testClienteAntiguoConDosFechas() { // Cliente m�s antiguo con dos fechas iguales
		gestorPruebas.getListaClientes().clear();
		Cliente esperado = new Cliente("cliente", "5sdf55sd", LocalDate.parse("2002-10-01"));
		gestorPruebas.getListaClientes().add(esperado);
		Cliente cliente2 = new Cliente("cliente2", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gestorPruebas.getListaClientes().add(cliente2);
		Cliente cliente3 = new Cliente("cliente3", "5sdf5d", LocalDate.parse("2002-10-01"));
		gestorPruebas.getListaClientes().add(cliente3);

		Cliente actual = gestorPruebas.clienteMasAntiguo();

		assertSame(esperado, actual);
	}
	
	@Test
	public void testClienteAntiguoConFechaDuplicada() { // Cliente m�s antiguo con fechas diferentes
		gestorPruebas.getListaClientes().clear();
		Cliente esperado = new Cliente("cliente", "5sdf55sd", LocalDate.parse("2015-01-01"));
		gestorPruebas.getListaClientes().add(esperado);
		Cliente cliente2 = new Cliente("cliente2", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gestorPruebas.getListaClientes().add(cliente2);
		Cliente cliente3 = new Cliente("cliente3", "5sdf5d", LocalDate.parse("2002-04-01"));
		gestorPruebas.getListaClientes().add(cliente3);

		Cliente actual = gestorPruebas.clienteMasAntiguo();

		assertSame(esperado, actual);
	}
	
	// 2 METODOS CLIENTE M�S ANTIGUO
	
	@Test
	public void testEliminarClienteInexistente(){ //Probar a eliminar un cliente que cuyo dni no existe, debe de dar false
		gestorPruebas.getListaClientes().clear();
		gestorPruebas.eliminarCliente("456789123-A");
		boolean actual = gestorPruebas.getListaClientes().contains("456789123-A");
		
		assertFalse(actual);
	}
	
	@Test
	public void testEliminarClienteExistente(){ //Comprobar que elimina un cliente 
		Cliente clienteDNI = new Cliente("Mar", "88896", LocalDate.now());
		gestorPruebas.getListaClientes().add(clienteDNI);
		boolean actual = gestorPruebas.getListaClientes().contains(clienteDNI);
		
		assertFalse(actual);
	}
	
	@Test
	public void testEliminarClienteSinFacturas() { //Eliminar cliente sin facturas
		gestorPruebas.getListaClientes().clear();
		Cliente esperado = new Cliente("cliente", "456789", LocalDate.parse("2010-23-07"));
		gestorPruebas.getListaClientes().add(esperado);

		gestorPruebas.eliminarCliente("456789");

		assertFalse(gestorPruebas.getListaClientes().contains(esperado));
	}
	// 3 METODOS ELIMINAR CLIENTE

}
