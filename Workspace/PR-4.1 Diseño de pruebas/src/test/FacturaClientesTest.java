package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class FacturaClientesTest {

	
	static GestorContabilidad PruebaGC;
	
	@BeforeClass
	public static void antesDeNa() {
		PruebaGC = new GestorContabilidad();

	}
	
	@Test // Asignar una factura y un cliente inexistente
	public void testAsignarVacio() {
		PruebaGC.getListaClientes().clear();
		PruebaGC.getListaFacturas().clear();

		PruebaGC.asignarClienteAFactura("", "");
		
		assertNull(PruebaGC);
	}
	
	@Test
	public void testAsignarClienteSinFactura() { // Asignar cliente a Factura Inexistente
		PruebaGC.getListaClientes().clear();
		PruebaGC.getListaFacturas().clear();
		Cliente unCliente = new Cliente("Cliente nuevo", "123456a", LocalDate.parse("2082-07-10"));
		PruebaGC.getListaClientes().add(unCliente);

		PruebaGC.asignarClienteAFactura("5sdf55sd", "");
		assertNull(PruebaGC);
		
	}
	
	@Test
	public void testCantidadFacturasPorCliente() { // Numero de facturas por cliente
		PruebaGC.getListaClientes().clear();
		PruebaGC.getListaFacturas().clear();
		Cliente actual = new Cliente("Cliente nuevo", "12378946a", LocalDate.now());
		PruebaGC.getListaClientes().add(actual);
		Factura factura1 = new Factura("23154a", LocalDate.now(), "ps2", 85.5f, 20, actual);
		PruebaGC.getListaFacturas().add(factura1);
		Factura factura2 = new Factura("78945sa", LocalDate.now(), "psa2", 29.4f, 230, actual);
		PruebaGC.getListaFacturas().add(factura2);

		int esperado = PruebaGC.cantidadFacturasPorCliente("5sdf55sd");

		assertEquals(esperado, 2);
	}

	@Test
	public void testEmptyCantidadFacturasPorCliente() { // Cantidad de Facturas por cliente sin facturas
		PruebaGC.getListaClientes().clear();
		PruebaGC.getListaFacturas().clear();
		Cliente da = new Cliente("Cliente nuevo", "12378946a",  LocalDate.now());
		PruebaGC.getListaClientes().add(da);
		

		int esperado = PruebaGC.cantidadFacturasPorCliente("5sdf55sd");

		assertEquals(esperado, 0);
	}

}
