package clases;

import java.time.LocalDate;
import java.util.Scanner;

public class Cliente {
	private String nombre;
	private String dni;
	private LocalDate fechaAlta;
	private static Scanner IN = new Scanner(System.in);
	
	public Cliente(String nombre, String dni, LocalDate fecha){
		this.nombre= nombre;
		this.dni= dni;
		this.fechaAlta= fecha;
	}


	public void rellenarCliente(){
		
		System.out.println("NOMBRE_");
		nombre = IN.nextLine();
		System.out.println("DNI");
		dni = IN.nextLine();
		System.out.println("FECHA (YYYY-MM-DD");
		String fecha;
		fecha = IN.nextLine();
		fechaAlta = LocalDate.parse(fecha);
	}

	public Cliente buscarCliente(String dni){
		return null;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
}
