package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;
	
	public GestorContabilidad(){
		listaClientes = new ArrayList<Cliente>();
		listaFacturas = new ArrayList<Factura>();
	}
	
	public Cliente buscarCliente(String dni){
			
		for(int i = 0; i < listaClientes.size();i++){
			if(listaClientes.get(i).getDni().equals(dni)){
				return listaClientes.get(i);
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		for(int i = 0; i < listaFacturas.size();i++){
			if(listaFacturas.get(i).getCodigoFactura().equals(codigo)){
				return listaFacturas.get(i);
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente cliente){
		if(!listaClientes.contains(cliente.getDni())){
			listaClientes.add(cliente);	
		}else{
			System.out.println("Ya hay un dni as� en la lista.");
		}
	}
	
	public void crearFactura(Factura factura){
		if(!listaFacturas.contains(factura.getCodigoFactura())){
			listaFacturas.add(factura);
		}else{
			System.out.println("Ya hay un c�digo as� en la lista de Facturas.");
		}
	}
	
	public Cliente clienteMasAntiguo(){
		int anioMax = 0;
		int anio;
		if(listaClientes.size()>0){
			Cliente posicion = null;
			for(int i = 0; i < listaClientes.size();i++){
				anio = (int)(listaClientes.get(i).getFechaAlta().getYear());
				if(anio > anioMax){
					posicion = listaClientes.get(i);
				}
			}
			return posicion;
		}
		else{
			return null;
		}
	}
	public Factura facturaMasCara(){
		float max = 0;
		Factura aux = null;
		for(int i = 0; i < listaFacturas.size();i++){
			if(max < listaFacturas.get(i).getPrecioUnidad()){
				max = listaFacturas.get(i).getPrecioUnidad();
				aux = listaFacturas.get(i);
			}
		}
		return aux;
	}
	
	public float calcularPrecioAnual(int anyo){
		
		float precioTotal = 0.0F;
		
		if(anyo > LocalDate.now().getYear()){
			return 0.0F;
		}else{
			for(int i = 0; i < listaFacturas.size();i++){
				if(anyo == listaFacturas.get(i).getFecha().getYear()){
					precioTotal = precioTotal += listaFacturas.get(i).calcularPrecioTotal();
				}
			}
			return precioTotal;
		}
	}
	
	public void eliminarFactura(String codigo){
		
		if(listaFacturas.contains(codigo)){
			for(int i = 0; i < listaFacturas.size();i++){
				if(listaFacturas.get(i).getCodigoFactura().equals(codigo)){
					listaFacturas.remove(i);
				}
			}
		}else{
			System.out.println("No existe");
		}
	}
	
	public void eliminarCliente(String dni){
		if(listaClientes.contains(dni)){
			for(int i = 0; i < listaClientes.size();i++){
				if(listaClientes.get(i).getDni().equals(dni)){
					listaClientes.remove(i);
				}
			}
		}else{
			System.out.println("No existe");
		}
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		for(Cliente unCliente: listaClientes) {
			if(unCliente.getDni().equals(dni)) { //si existe el dni del cliente
				for(Factura unaFactura: listaFacturas) {
					if(unaFactura.getCodigoFactura().equals(codigoFactura)) { // y existe el codigo de factura
						unaFactura.setCliente(unCliente);
					}else {
						System.out.println("No existe la factura");
					}
				}
			}else {
				System.out.println("No existe el cliente");
			}
		}
	}

	public int cantidadFacturasPorCliente(String dni) {
		int contador= 0;
		for(Factura unaFactura: listaFacturas) {
			if(dni.equals(unaFactura.getCliente().getDni())) {
				contador++;
			}
		}
		return contador;
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	
}
